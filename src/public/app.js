var app = angular.module('myapp', ['ui.router'])

app.config(function ($stateProvider) {
  var reset = {
    name: 'reset',
    url: '/reset',
    templateUrl: 'reset/reset.html',
    controller: 'ResetController'
  };

  var login = {
    name: 'login',
    url: '/login',
    templateUrl: 'login/login.html',
    controller: 'LoginController'
  }

  var loginCode = {
    name: 'login-code',
    url: '/login-code',
    templateUrl: 'login/login-code.html',
    controller: 'LoginCodeController'
  }

  $stateProvider.state(reset);
  $stateProvider.state(login);
  $stateProvider.state(loginCode);
})