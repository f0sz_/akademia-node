angular.module('myapp')
  .controller('ResetController', ['$scope', 'FormsService', function ($scope, FormsService) {
    $scope.email = '';
    $scope.newPassword;
    $scope.res = null;
    $scope.resetPassword = () => {
      FormsService.resetPassword($scope.email)
        .then(res => {
          $scope.res = res;
          $scope.newPassword = res.data.payload.verification_code__c;
          console.log($scope.res)
        })
        .catch(err => {
          $scope.res = err;
          console.log($scope.res)
        })
        $scope.email = '';
        
    }
  }])