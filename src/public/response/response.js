angular.module('myapp')
  .directive('response', () => {
    return {
      restrict: 'E',
      templateUrl: '/response/response.html',
      scope: {
        response: '='
      },
      controller: ($scope) => {
        console.log($scope.response)
      }
    }
  })