angular.module('myapp')
  .controller('LoginCodeController', ['$scope', 'FormsService', function ($scope, FormsService) {
    $scope.email = '';
    $scope.model = {
      email: ''
    };

    $scope.QRCode;

    $scope.getCode = () => {
      FormsService.getCode($scope.model.email)
        .then(res => {
          $scope.QRCode = res.data.payload.QRCodeUrl
          console.log(res.data.payload.QRCodeUrl)
        })
        .catch(err => {
          console.log(err)
        })
    }

  }])