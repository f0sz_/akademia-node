angular.module('myapp')
  .controller('LoginController', ['$scope', 'FormsService', function ($scope, FormsService) {
    $scope.email = '';
    $scope.password = '';
    $scope.res;
    $scope.login = function () {
      FormsService.login($scope.email, $scope.password)
        .then(function (res) {
          $scope.res = res;
        }, function (res) {
          $scope.res = res;
        })

        $scope.loginForm.email = '';
        $scope.loginForm.password = '';

    }
  }])