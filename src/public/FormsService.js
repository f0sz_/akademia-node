angular.module('myapp')
  .factory('FormsService', ['$http', function ($http) {
    return {
      resetPassword: function (email) {
        return $http.post('/api/v1/reset-password', { email: email })
      },

      login: function (username, password) {
        return $http.post('/api/v1/validate', { email: username, password: password })
      },
      
      getCode: function(email) {
        return $http.post('/api/v1/login-code', {email: email});
      }
    }
  }]);