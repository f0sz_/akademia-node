const express = require('express');
const router = express.Router();
const database = require('./database');
const helper = require('./utils/helper');
const gpc = require('generate-pincode');

router.post('/validate', (req, res) => {
  if (req.body.email && req.body.password) {
    const email = req.body.email;
    const password = req.body.password

    database.task('test', async t => {
      try {
        const user = await helper.validateUser(t, email, password);

        const officePromise = helper.getOffice(t, 'Office Krakow');
        const entryPromise = helper.getEntry(t, user.sfid);
        const [office, entries] = await Promise.all([officePromise, entryPromise]);

        if (entries.length > 0) {
          return await helper.updateEntry(t, entries[0].sfid);
        } else {
          return await helper.insertEntry(t, user, office.sfid);
        }

      } catch (e) {
        res.status(500);
        res.send({
          action: 'validate',
          success: false,
          response_date: new Date(),
          payload: e,
          errorMessage: e.message || 'Unknown error'
        })
      }
    })
      .then(entry =>
        res.send({
          action: 'validate',
          success: true,
          response_date: new Date(),
          payload: entry,
          errorMessage: ''
        }))

  } else {
    res.status(500);
    res.send({
      action: 'validate',
      success: false,
      response_date: new Date(),
      payload: null,
      errorMessage: 'You have to send email and password'
    });
  }
})

router.post('/reset-password', (req, res) => {
  if (req.body.email) {
    const email = req.body.email;


    database.task(async t => {
      try {
        const user = await helper.getUser(t, email);
        const pincode = gpc(8);
        const updatedUser = await helper.updateVerificationCode(t, user.sfid, pincode)
        res.send({
          action: 'reset-password',
          success: true,
          response_date: new Date(),
          payload: updatedUser,
          errorMessage: ''
        })
      } catch (e) {
        res.status(500);
        res.send({
          action: 'reset-password',
          success: false,
          response_date: new Date(),
          payload: e,
          errorMessage: e.message || 'Something went wrong when resetting verification code.'
        });
      }
    })
  } else {
    res.status(500);
    res.send({
      action: 'reset-password',
      success: false,
      response_date: new Date(),
      payload: {},
      errorMessage: 'Email field is missing'
    });
  }

})

router.post('/login-code', (req, res) => {
  if (req.body.email) {
    const email = req.body.email;
    database.task(async t => {
      try {
        const user = await helper.getUser(t, email);
        
        const QRCodeUrl = await helper.generateQRCode(user);

        res.send({
          action: 'login-code',
          success: true,
          response_date: new Date(),
          payload: {QRCodeUrl: QRCodeUrl},
          errorMessage: ''
        })
      } catch (e) {
        res.status(500);
        res.send({
          action: 'login-code',
          success: false,
          response_date: new Date(),
          payload: e,
          errorMessage: e.message || 'Something went wrong when generating QR Code.'
        });
      }
    })
  } else {
    res.status(500);
    res.send({
      action: 'login-code',
      success: false,
      response_date: new Date(),
      payload: {},
      errorMessage: 'Email field is missing'
    });
  }
})



module.exports = router;