require('dotenv').config();
const path = require('path');
const express = require('express');
const bodyParser = require('body-parser');
const cors = require('cors');
const app = express();
const database = require('./database');
const routes = require('./routes');

app.use(express.static(path.join(__dirname, 'public')));
app.use(cors());
app.use(bodyParser.json());

app.set('port', process.env.PORT || 5000);

app.get('/', (req, res) => {
	res.sendFile('static/index.html');
})


app.use('/api/v1', routes);

app.listen(app.get('port'), () => {
	console.log('Example app listening on port: ' + app.get('port'));
});
