const database = require('../database');
const QRCode = require('qrcode');

module.exports = {
  getUser,
  getEntry,
  updateEntry,
  getOffice,
  insertEntry,
  updateVerificationCode,
  validateUser,
  generateQRCode
}

function validateUser(t, email, code) {
  return t.one('select * from salesforce.User where email=$1 and Verification_Code__c=$2 and isactive=True', [email, code]);
}

function getUser(t, email) {
  return t.one('select * from salesforce.User where email=$1 and isactive=True', [email]);
}

function getEntry(t, userId) {
  return t.any('select * from salesforce.Presence_Entry__c where employee__c=$1 and start_date__c::Date=Now()::Date limit 1', [userId]);
}

function updateEntry(t, entryId) {
  return t.one('Update salesforce.Presence_Entry__c set end_date__c=Now() where sfid=$1 returning *', [entryId]);
}

function getOffice(t, name) {
  return t.one('Select * from salesforce.Account where name=$1 limit 1', ['Office Krakow'])
}

function insertEntry(t, user, officeId) {
  return t.one('insert into salesforce.Presence_Entry__c(name, office__c, employee__c, start_date__c, end_date__c) VALUES($1, $2, $3, Now(), Now()) returning *',
    [`${user.name} entry`, officeId, user.sfid])
}

function updateVerificationCode(t, userId, code) {
  return t.one('update salesforce.User set Verification_Code__c=$1 where sfid=$2 returning * ', [code, userId]);
}

function generateQRCode(user) {
  return new Promise((resolve, reject) => {
    QRCode.toDataURL(JSON.stringify({email: user.email, verification_code__c: user.verification_code__c}), (err, url) => {
      if (err) reject(err);
      if (url) resolve(url)
    });


  })
}